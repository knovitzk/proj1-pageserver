# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

The objectives of this mini-project are:

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

## Author ##
Kyra Novitzky

## Contact Address ##
knovitzk@uoregon.edu

### Description ###
This project extends a tiny software in python to be able to
a) identify if a prompted URL ends with .html or .css and, if so, it sends the content of that file with a proper http response.
b) Identify if there is no said .html found in the current directory. If not, it will respond with a 404 not found error.
c) Identify if a page starts with ~, //, or .., if so, it responds with a 403 forbidden error.
